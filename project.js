//!wrt $BSPEC:{"frn":"StudentVue","dsc":"StudentVue client for Windows96"}
var folder_exists=await w96.FS.exists("C:/user/appdata/StudentVue");
var login_exists=await w96.FS.exists("C:/user/appdata/StudentVue/LoginData");
var server_data=await w96.FS.exists("C:/user/appdata/StudentVue/ServerData");
var loggedIn=false

var iconSmall=`data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAKg0lEQVR4Xu1Za2xT5xl+zjn28bFjO3aubho3JdzSlpAUKCNQoBudYAUKW9lA7EcpaIhOa7UV1klVW7SOCanqn+7Cn7KtEkgwutGJXqjKNkFbOgaMTMAKhGtCwAm5Or6f697PNiH4egLR/ozXsuLY3/d+7/O99/cA9+jeDdy7gf+LG+DGGuXs2bNfTiQSjxPfcfSW7Xb7UENDw6mZM2fuW79+/V/H+ryb/MYEyPTp0+8nhrth4HHBEGDVrfQRUHkFGqcB6VN4nsfcuXN3b9iwYc2kSZMSYwnqroDMmDFDMnQjJmoiyhIVcKseWA3rbfIpnIKQZQgDYi/ilvgwqJGLvF4vGhsbjzzxxBObli1bdvhOAN4xENLCHzidW1Mduw9lSgV4ehUig3QUtAwgYL8OTVDzLnU6nVixYsXrL7zwwi9GA+iOgBCIU7wuTKkLj4NDLyHLMc9G5mSI1WfxTG0YbgsQIkxdMo+2MI9jQQtCWupCfD4ftmzZUvnoo4/2mgFkXoI0NwKxjTTxfF2kHk7NZeaMrDUKgXFXn8dSfxDTSnVwJIVBThXXgS/7BewJiOhM8BBFGzZv3nxj0aJF1cUOGj2QadMNX7QG5UrlqDSRKYgCGRdc5zC3MoYfj5NhHWGZCYoPOzqt+MsNERwvYOvWrXjyyScLylrYsDNOJ20YdtVBICruCgRja4UIX6wGB/useKeDohwLc2myCcC6BxRseCAOXdeYiaG9vX3Eimz9mAZCICQWUyviVQSCh04v5rzXpU502jvQI3YjwVFUGgV51DJYNRs+7hFxLWMrM7fFVRqWVsoIhUJJMIXINBBi0iXoFrg0N+J8DJdKzuOqsx0Dzl4MufrRXRLAefdZXJOuQqOXGWJBwq2U0v1wOE6OnkkMzLO1CqpFDSdOnMDBgweH8vHN3p1fglKX4kqCuFHfDu9kHiU+J6wlqbvQEgaG2mXcONEHhZdRF603ZX6SLiX3d5Fz5yI7Sfhdn4zfdEjYvXt33uhiSiNUXixnh+jlMvSlXfA/JcE7wQbRKVDEoXult0XiUTZZwrjFTsRKQ0lTM0MsvzByCfldoMWrkTEbSa309fX5c/E1BUTTtPfLp4io+Y4Ax4N8UvB8ZHML8H3Njh6pGyxnFKOoEKUlBhpd+ZNkKRULEx06SA4cP3782TsCwiJV9UwbqqZJ4Hlz0drlt4IvMRCgQHDzxnMdnuASCIqDqLVpeNiVXyPs3mokSjJEbW1tG0YNhEB0lT1iRVmDraAWMhkzwLZSHiFxCN1iICcYFSquOq7Q5Wh4vk6GUMQ2bOnfe3p6WIGaRXmdfcGCBe+FlWB1ZZM0KhDsBIOSAoX/ZIHYa7+BmCWKMrkCkiZB5wxELCH02nqgCjLW1SbQ5KbsXsQGtbTCFEXJuTLnPezbt2/m4ODg45XNNggjU24xg0//ziJYoi9lCkzCiBimUH0F50vP4qL7HLoc1wmEgrleBct9arJEKUQsWQ6pqUWiKJoHQvH627wVPnfd7SW5GRxMG73/ScBI48i3R+R0/MAvw4zbqcTrVIjSPVFFRYV5IK2trY85qgUIFlNBbZgxA9F/JoH+08WjVbNbQ1nuy80S9ETIgoie0kg4HDYHhJyJGxoaetDmpRsoZrhpf1CiOgYvyWj/NILuY+Yav/slyuem+AN/1qZBrWhKAjhw4ADIT6ZloslydqprPLTIK0j5T2E3L4d09J5KIHyV2llzst929gD5LPPfYlj+HeTRqtdCu388LL0nEQwGcfr06X9lbs0VtVi8EfLZOAMRvCwj8I84HHELFob8aIh5kwJ9ZR/AZ64ARaX8ye0mGlZbKRo1WSnTz0kDZKFvX7FBn1AJw1UDw+oEp4Rx5syZrPVZTlBfX88Ks4gylNtbQx0Krn8RR0PQg191zMGavga0RH2YRe/n6PPb9N1DUabUwhTWOPwxYLmtfB+5I07X+csLErqpA9W94wHqS/SSVH/V2dlZHAhbQX3z2WiPmswHI0mnYN59PA6PLGJTdxPcOiXKEcbBPpfSdz8NNMOr5PZkxjHFlcPugA27rlnAGqmbR7G/3VTSv37Ohq8ipLXauaBCLrnDkMqSf2nclAUkZ0Jsamo6cvjw4W/E+lQ4Km6F4PB1BUrYwJLBOrhI4HzkpEnKkoE67Kg6P7xEk8qh+udDd1PNx1GxGe2BJfBP7Ay0YX+vRrWWTj28gfYo9e/0juk8FN9MaP55t47hU+JaLNli5wQyb968PxGQH/aeTHj8X7cMZ3Y5lLrLhpinoJMyzUyOe4YFULyToDy0miS4pSXDUQG5vAFcpAvd14+gZ/BS0v7Bi9A9tVBqWmAwkxoZ2rRUVi8vLzenERrHtM6fP/9Y+Gr4m6FOBW5/SgCBKYewlFFXV4xKadbFSLeUQGlYeRuI4b0kpOG8D8rE5emvRsSxzNhMNsfFUgOViRMnZs2+8ma8p59++h3aEw58GYMcSXV8juqUAiW9QKjJQKhWU8i32gvjZkIn3yTOzc+ZO6h440l7giCgubl5VebPeYFs3LjxPZot7dfI8ToORJNgRBcPp19IO2th2WJU1SY1QmFzLIgbvAiOhJk1axZoMpkVtgrWIB999NH3rFbrCTmoo/2TCOL9Gsobbehlo88ipFEtlaSMyFdsX87fiYe18/OkX65cuXJhrjVFi6lXX311NqkzxKLV5Y8jCF5SqGFiXV1hqlTsoIodfCg75hfbm2U2PachBC+Bpo4fzpkz59M7ArJkyZLE0aNH3Q6Hg+wEGDynoFXqLdj5sYNcFIL9iRJYulvp4UJktLIPr+fCAYjn34dks53dvn370nyMimrk5sZ3333XRUOI5L9HS24gTFP2QsRC8DP99eDVGMQzuwCluBZv48eiFIVk28nfwWLIba+99lpj4fNGcVfU/FtXr14tX7hwAcv66vD9gUkFRz6sX/99+Rl84umEbiuFXP8U9MpHUtGpEKlxWDoOwXrtCzYKvLhp06YFq1atah8zIIwRJcrtL7744jqRMu/POx/DeNldEIxOYD4ovYIPPTTMs9A4yVEJraoJnOtBKHYvYHOziEDmF06GV56iEzNHTo3C6/Ecf+WVV1qo7S5ahRaronNewltvvWXs2rUrWU+9QWCqNUdRzajkYFfEEA34NLg1K6pUO970ncQpVzBVfBnU8qZPY0+2Fi5c+CaNSX9m1mDuCAhj/tJLLxmHDh1ChSzhJ12NmCizssUcuwE+jh3lbfjc3TXckLBZGWVskB++TU+tXqYqvHibOQKluZPzXAuFZmP//v0QqA391qAfywbHUfVLjwJyAGL+MkSj1L+5OvGBtx3hdM/i9/tBjrx08uTJn1HVnXe2W0wzdwWEMd+zZ8+vt23b9iM2MbfR06bmaDkejpWhmvKISA9GY7yKa2IE56RBnHL0Q+ZTiZJpYPHixUGqICa43W5TT6XG1NlzMevv7/fu3LmzZ+/evQIDVIjST3axdu3aBVOmTPl7sZs2+/tda2TkQfF43E7J8zkaNv/28uXLIIBQVRUejwc1NTWYOnXqGy0tLTuqqqoumBXQ7LoxBZJ5KHWYrExmZ+hkSkUmXWZFvrfu3g38T2/gv2o38Op9P69EAAAAAElFTkSuQmCC`
var iconLarge=`data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAA9b0lEQVR4Xu19B2Ac1bX22V6kVe+WZIw7dkw3xhRTzDM9EEInlEAgCYTAH0JCwktiXkIKBPPyAsS8JGB4hF5CMNWGgAEbMDbNxgU3SbZ6WWl7/b8zu2urzO5smVmN5LmwluyduXPvufebU+85RFrTKKBRQKOARgGNAhoFNApoFNAooFFAo4BGAY0CGgU0CmgU0CigUUCjgEYBjQIaBTQKaBTQKKBRQKOARgGNAhoFNApoFNAooFFAo4BGAY0CGgU0CmgU0CigUUCjgEYBjQIaBTQKaBTQKKBRQKOARgGNAhoFNApoFNAosF9TQLdfz16Byb///vvmaDTKdI20tLRMw9+vDAaDBfh7tLq6estxxx33qMVicePv+mOOOcarwBC0LmWkgAaQLIn52muvNezYsWPO9u3b527ZsmWe0+ms8/l8pQBDIbo04mMAUKzDu9fpdFH8GwMjotfrfSaTacDhcLSVl5fvOuSQQ1Y0NjZ+duGFF36U5bC022SmgAaQNAn60UcfGd9+++1LN27ceNrmzZuPBBga40BIs4f0LzMYDN2VlZW7Dj300JdnzJjx8mWXXbY6/bu1K+WkgAaQFNT84IMPbG+88cYV77333rc6OztngiOUykn8dPsyGo3bDjrooLeOPfbYR66++upV6d6nXZc7BTSAiNBw2bJlJyxfvvxnO3fuPCocDhflTmb5eigqKvpi3rx5j5577rl3zZ07l8U1rSlIAQ0gg4h72223/fSdd965CuLTNAVpLkvXEMOap06dyuLX4tNOO61Vlk61TkZQYL8HyJtvvln1j3/8Y/Enn3xyOkQo1ivGWnMWFBR8smjRoqU///nPHx9rg1f7ePdrgPzgBz+4Y82aNd+JRCI1sixUOgKPghQHUFaBo/z42muv/UCW+WidkILLpV7qLl269IxHH330916vd1bGo0yAAD8NUQMZIkbSR/VkipjJEraQLWInY5StvNz2kTdKEfLqvRTSByigD1JYF8LvIdh6wxTC73svzXFFYEZ2H3HEEUvPP//8/zz55JM9EBn1eAEYwCEXffjhh+fhdweUfvfRRx/9FBT/dyCq+Xmgxx9/PP/U2jAK5LgcY4uer7/+euO99977SHt7+4KMRw5A6OD/s4Zswqcw5BDAAIhgb8fIOPzn4GdEaR97SfzOPyMCcDw0YHSSz+jDx0tRwVUyBF8ZDxc3BPDx4MOdWfCxi3TC3/nwCcEn4wFwnHa7ve3AAw/8aPbs2a/V1dU1AWibs3n4eLlnvwHI3XfffcFTTz11L6xStWkvHm8ffCwhCwBRRMXBErJGbSmBkHbfgy4cDBifzktug4u8Rg+5TAPgMJHBjCib7nO6B47M5tra2k2zZs16FRzntUsuuWRDTh2OsZv3C4DcfvvtP4Ln+3cQLxKyT+pligOjMFBEJQBFQdgB13js1gSXUGqdh4Olz9RLTnMvhQ1hObhKzsO2Wq2fQIT7vzPPPPPBU045ZSDnDlXewbgHyDe+8Y3lu3btOj2tdWAxKqIjR7CYSgNlI0SotPqQ8SIGCwthAQqSy9hP3ZYuChpZchKQOqoNus5ARUXFWuguS3/2s589OaqDUfDho0xm5WYGfWPir3/965fdbvdBkk+Ji/y2oJ2qfDVkjxSQHv8pzS0kxxW/IAaUKIXxX7epk3qsXaMueg0eOxT9jYgje+TBBx/8fbpzGivXjUuALFmy5FtPP/30XX6/vzrlQsSBYQqbqcxXQaWhsiFKt9oWMSF++XU+AKWL+iy9FGUdRQUchYcARb955syZy2E9u/+KK674XG30y2Y84w4gP0VD/NRv0yIG9laRv5iq/LUw81hUwzHSGTtbv1z6AWq3tpIf1i8wPDU1Fyxhj91www3XL1iwIK48qWl46Y9lXAHkRz/60a///e9//1xy+nFdo9JXTeXBCuytfaZayXtVdAFzlCA0lHZLGznBTVQGEoLZeAc8/L+54447/qYismU0lHEDkFtvvfW2lStX3ik5e3bwhQ1U451AReFiVekakmMXuSDhS+k1dlOHrW2YbhKTIU3wq1j0UTJjtQsM/HvMORLEHx683/0wTPjwCcHPs89bI9/WKC0tfe/yyy+/Hp9Ps5njaN4jHxVGcRaIpTr2j3/845u8F6R0DnPITHXeBiqIFI4pkUqKvCxyufUu2mNroajJRxNtEWqwhumgwjAdYItSpTlKVgOYDFacpbG4JZvC+CUAUbPdr6M9Ph31BnXUHtBTR0BHnfg5ENKRK6yDeSCxVbLbMrB6eXCCcvF///d//0FqLmr6PrvZqmgGOMRUiyjcNVDIUwcassMvaKV6b+MQZ5+KppLzUBgk/YY+sld9Rf9vUoAq4T83AQ06rPLehRZbcdBmUASN8BdW/UP4wxkk2uHR0YpuI33pMlI/wBIVestu68A7/yrW66z58+cjvkb9LbtZqmRe69evt958880fDwwMSJpyzQELNXgnjltw8JLs1UmszVRV0kfn1vjpqFKIVnHOkc2yRRks+AQBlm64YHZ59fRpv57WOI3UFUTHWQAFZ/I3ff/7378QgZWfZTOmfN4zpgFy3nnnvYpDTYukxCp2/jW6J1FhxDGuxCqxeSf8JT3GLuq3t9L8cj8trAjTlIK4iMUcJcMdJnCXBJeJA4bBsrzDQO/2mSCOGTLWXeA76YJOchEsXSszHE5eL8+UVnkdXKqHgU3fDmfgf0mBQx/RU7WnjkrDZYJCvr+0hBm4zbqHgiYvdJEQnVkVoiOKI2RF1IwgJGW5+oL+woDBxwVBabNLR2udBnqvz0h9ofS5CnvjL7room/fcsstz6h1XbIk0ehOB4rehY888sg/MIrkOz5uyq3zNFBxuGS/AkdidZibBMhPzbZd5DN5SAdLViMU90UVITq+LEzFMGkIIBmso2SxtAkxrBMB8y91GOmdHiN1ZwCUadOm/c/jjz9+YxaPVvyWMQeQd999txL+jrWhUCi1Ug6ZucJbRVXBmv0SHENBEqBm+06ABNmGBDBEaLItTN+uD9D0wpiOkgtHiUtgAkeJgO4t8Fvetd1CO30jz8Uk29FwLD6K6IfLFd/xGT5gzAEELPmfW7duPVtKtCrwO6jRN3HMOgEzXEcJckTJo/NQU8EOChsTxiP2i0TpyOIgXVoXpHobh4pkrp8Mf7AgfgEkPdBR3ukx0KtdRtrtZ6BIbzWcsf/7E088cbWcc8+1rzEllN93333npAMOY8hINQgfGase8lwXdfj9HHRpwzkWDsSEQyPedBTAScj3+sx05zYLrXfC14HvmAvk0hKcqBwm5q/XhOnnk/00wx7CCKQ7xtp+G0p7ar0yl8Flca80rLPoVKlbENfzhcvlSn1MFm+vCW7WO0r3a9Fq5Js9dnqx1byb+mw9w17oUSpA0OO1DX46oTxChrjIles6JnQT9qW80Gagf3WaAUpW4lO3k0466Yd33XXXn6Suy8f3Y4aDIMHCYklw4CVV7C9BCMn+qZSn2jDMRdiKVx2oEUJthr7QdeSGtW9ps4Xe7dFTSAZOwmNhAwCLbSVmoksnhOnGiX6qNLGIl5qbINPMknvuueeMfABA6hljhoMgSdp2HJedlHRCoLkxbKRJrimE9Anj3t8htbDJvmfLVjd8JG323SI2QPhKdBH6yYE+OqIE/nJW6GXaIQlustVF9Hso8B3B1HoJTi5+hYyWU7Odp1z3jQkOgrfJ5SnBwdQAQCp8VQjGMmvgSLE7mJOUhErJHkDC+REvcgQtQi95ts0khJgIvg6ZdlqCm0xFau+fHOinahMrQ8l7R/K+KTDIPC3T47PuZkwA5Lnnnrst5QxB54JAIRa+TANHGluBM7HU+OuInahiIPnCbaL7d5kEJ2CuSvvg4TAzMuCR0wCSWwCSKgmQQGn/5k033ZR67dOYby6XqB4gixcv/gHyV82QAkhZoELVpwFzWSS572UuwtlZSvxlSV7iOsRamWCm1Qt+Dbm4SGIezE2mwf9ydUOAbBDpUj1h1apVd/71r3+dLzcN0u1P9QDB6cDrpSZjCpuEOKtsW+LMt9jPbPtU+30MEuGlEhG3KkXw/bNtZmqDd5z9GnI2BghzkqNKYpYzswCS5A1J/h6Q8/mZ9KVqgCAJwFngHtOluEcJMpBkkmQhcciIkyAgzyF54UTrM/RQu7l176fT1EED+n585xVO7fG1g5O/ZUJkNV7LADHjP3sI8o4oi9BBkdbT+3D2yc1BmB4sbhmx+06siNDCcig8KZ4C6+WcH/7wh4tHg47p5YkajZHhmTjr8X2pR7PlqjRYnrbuwb4AH5IeOI295DL3C+k/w/wGS2QzHPxAjudi8yhkdU4tWhooR44sO5milowAKTWH0fo+xkXKacDkxCtdbBQ6WjdgoHOiYfGvcxx4gpN8syZEW90G2upNbtlavXr1rStWrLhv4cKFHTk+NqPbVctBkEvWASXtOCnuUeor35vULdW1zAH69U5qtu6kHYVbqdvWiWQH/lhCNgTx7T0DlDgLxD9Zh8V3YWNYiGNqtbfQNtzbZNtBTn0vOAu/+cZuY4DYkT7VEoHbOwmbaPUZyM1+EYWmyWQuh5/ksgkBsoi9pOLPhRXTev/99/9VoWEk7Va1AIHuwaZdLn6ZvOHF70B9m1Rh7CwW+fHfbkszNRXuoAFLP0VxLluIA87Exp8ADO51m13U4miiJvsO6jF0EdJQj1nxi8NxOINkMjHLiSO3TThRKKc1a/CCJrjIbEeUTirnpHjJ9REkADzr4YcfPiafIFEtQCBeXSXFPSxhK5kh7oi1xMEh5hq8kfutfZmDItkA4lzGa/bQnoIW2gPw+ZEDmsW3sdaYiziQdzhZC+L7txG+HlZwagwSjii+pC5EUxFlnIpfIeL37nzSWJUAQcHMkra2NsljtMWBUsG0O7zFzkEEqM28h1oKdpEfSQwy4haZrAAWt9/qpF0F2wVFfywq81YU401mzWJSfOEyCNlPlGwMkhKcTzm3Jgjmnlygw76Y97e//e0EJccyuG9VKukonnkqqj0hADtJix+GKgxzxeWhLSZS+ajF1kQ+c+z8Q1JGgAUxmHRkQLFmvZEDh4Zezj6AkAc6iA+9JksxEO8/aArSHkML+X1wgAWqx1QkMYtZbIQIC9WphzcdTL2c5QRxbtAVlGoCGUF/PvF4sCNE6wc4QY344j322GO/w5fzlBqL6gEC59CFUpO3BQvIgjff4Py5DA5U2KAWuzjXMABylhK98LFWGMlWbiCjDTl4kQtnRNxRPMwiEopSyIuUOh0h8rbj0xmh4ECSNxwWuNvaKYhaNQF4qlWU3zcVPQXHIcRVnyhAOEIe6X+QDkg5VT02On6CDa/si3E+5cstRoS9iAMENemPeuihh+ZdddVVa6T2Sa7fq5KD7N69e7bUxOzhfXU6+FoBHDDfNtt2UsAE71actgaUjSmsN5K92oB0OChiYMW2NSQIHwNGUg7DLzVcazADVMXwB0yxUDgQJveeEPVtDZKnTUQwB0h6Ld1kRKWFylB12uZnqfkq/b2ZLVkpWkApM9awZ/J6cIKJr4GLfNSfnIu8+uqrN+NWyRdprnRTnQ4C5bwGzkHJYprmyD7uEQOHdx84MCtrpZ5q5lvogNPsVHuUnUomWchcAI0F3ikkC4h/0iNfjLvAHwJOY7QYqBh91Z9gp5qjLYJ4NqLh+V32DkEnGSvORS4nl6pxwrl8NH4MpF6aV5I6bda2bdvyYs1SHUA2bNhwLGiUWtrF24xt9ywa7E1MgDPXQauf7BP01LDQRo0L7VSKN765ADUEBREq9sm1JfphrlIyxUwNJ8OPUD6sX/yVy6i12Vpx1NU9JkDCp0VSNd60eWIiAvc/uChCDiFzvfhToaNOeOaZZ2bmup5S96sOIJs2bfq65KC5aGbULGy8EJx1nNrGODFEjafaqOHEAiqsNZERdkO5QCE2nhhH0UOPMdLEUwqpbFY8l07iYixyBE7IVoyNx6h2TpLaRB3L55uvxu+xCrwiDytKzUVQNexapceUx2lLT+UPf/jDZQgpuEzqSt5sLkM/dZraqdmxk3QHeah+gZ3sFSboDDERKl+Nn8XcpOoQG5UdNCw1MIbB6Xa4lofafSQRXXI7Lnu4C9m5mqfGq8fBjAcXpfaJbNy48VSlh6QagKDI5llwAv0FdQQl58zhH22lLeSe1knlp0ap9mjbII4hebvsFwiHgaDbVB5spaLJw+wegmWrS8gqomYuEtIlD5spNkLcGcYgZSfisA4ZJAfaIynDT3CoasayZcvmKDkWVVixwDlOR+KwZZho6tASXKCDLlmKN3XJZDOZCmIWqXxyjFSLwb6U6iOsFHR7yDvIwsVVoNpsu2mSZzLixlInoFdysZP1zcDlqrriLUpT7WEqHIWdUg3DWqU5Qi3wwyTzicBn9g18+ZlSdBt1DvLKK69UP/vss/dhgqVSkzSX6Kj+JKSvgThjKTLutUhJ3Zev7xPiVvXh8EwPtm7hdchVoHpQw0ONXITFv6BenIPwm/yoEkTz5nun4ME2vAwPdqR24X/11Vf/oeT65nvaQ+aCQ/mWO++88yVkSTwg5SRBrOKpRmo8JaaAx6xSSpIl+755bNYyA5XNHmaIw3h7rN2Cwq62FtD5hbB/sVZoiNBBDj4+ld8m6CH44+jSMLhucv2nt7dXUUvWqAIEVWhf8Hg8R6QiPYtU5bNNVHOklUzs9VYrMgZNgsdYfCCOI4Hj7W34NWQIqk5hZ47mNPYlFa/KTBEhh2/eEcKPBM2mwWlYZkoOEOisJQ888IBiPpFRA8j1119/a0dHR0orhB4v4Zp5FqqA8stK8FgAB+80HqcJdtGC2pGCew9qnbNTUy2iFgdXDuDgWDIATIaizKXb8s1BYnSEwxcvSE64napt3rx5rlL8bVQAgqO0R6xZs+YXqSbFcVN1x1vxJoa3Os+mW1mIjcUtmYoURIN1csE3EkHRzVZVRP0ySPtwsjJg2BeaM3junFDh1ApE144GOuID4cRzxSk4CF+2ffv2o2RZM5FO8g4QhLKbcejlIYwlqcWKFdwJC2zkqDML+sZYbMxFzIV6MjtGetn5wFWfATXO8+ebHkHCmJM1JMSNibOHKCJrQzQZqzTaUm2JMbUPBnrINKX2SN4Bgtoe96OeYNJgRD1Me3XH2aig0jRmRKpki6ODlsmBkiMaMNNl7RjVI7sMkB44MP0G8bMyRli2TkYdEa5xOCry1SCiCWNI0eAPmTAuAAKl/Lwvv/wyaXp75hz1JwAcNQDHGOUcgxeKuYijgQ+dDFs+VtiNQdptax6V47ps1nWi2CeDVHzzx0SrOTgGy8sw2jycS1inigSDol6FsJMyJUCSNw7y8ccf6+Hz4IMuok2HF231XKsQkj7aLF0uQgvnrS2I2RILlMV3HpOLOsyobY7/8iVu8XM8ereQm5cjEsR2/ww4Bs+vVQf34LWwCwBJ3RABXix1TTbf5w0gS5Ys+RPnWxVHB0y5XzNRUWNMrBor1qp0CG6ACUifzHnOvhFYtTiZdCJpXTp9ZntNQu/YY22hsB6WIRHWgFyKdFplkMpgQRTC/LN9mIz3SYlY/Kiurq5aGR+5t6u8AASHWxoRpXtlsgkU1OupbAbC1+Nh6UpMdLT6FOaUTFzk3YcV6LS1UZcxdhJRKU6SOKe/GyWiBx8o20cXfktH6DAcVDoSGQ9HnLAcLQLiuXvPt6UYQ09PjyIAyUuEzd///vc7Eb8varVi0YqD/Ax4TYwnzjF046VYWYCERZ2Oglby+71U66uXNcdwgjP54S3fzef043UKR44I5diKgvTdxiA5RskxmAsGkSJqZIKCXDqM35sXgMBOfXKysZZON5G1NHZ2Yzw2zmsrmds2PnWnpY9CCDuvRqk0Ti7NB8IGn7nPhD4JThSBx6XX2ENdlk7BMJDMpNtgCdM19UGqhBVRLaJVYr5haRUEebtwQEiBpriIdccdd3wPg0dxvJHNVIzUlzMhWo1TcPCMOelDNJ0V5osBFLd5QEghxLm2+Ix9pmmEGBgsqgk+Dhz53WnfDoUch7aSggMOKUQbX4HMhjVwzqoNHEyWgPQJCAoGg2KHn3OGjOIcBOa3H6biHkJWkXEOkEjqg3FDyQOQcKpTdiT2m/qpMIi6J8Ey4qQKSDkxJFNKgrskxCgGE5/rcJr6qM/UK8R+CRwjBXPmzOrX1Puhd0SFiF018nFvWHpUMPUq8rJXFCA453Hxk08+KZqd3VqFM92TmXvkDHLVdgDOSYF+KN7ZJF3jsBQApd/gFBLT6bD+xqiROJukCRlT9EiykDiTH4ZYFkK4us/gFaxTfB5eeqfDxwFuc151vHCnSsHB2yMgpP9JvVEsFgvnLZW9KQqQt956S5R7cIRu1aGIsUImgPHMPRgg/Tu5llkO6xbfF1HEcHEZhqBRrn0QpWNKgshkiHByrmqr0hcVk86XxgsG+QEy4dNpL4gibImfvnLlyrrOzk7RIDJ7rYFsSNw2nsHBoIggmZSnI43VTXu55LoQx5RxUu8KKOV8KEmt4ODZctLsTiFpXeqGop8DUtdk871iAHnppZeu4zfoiIa5ls9CEGI6xu1sZqSSe4TEEq1Qld0qGdCgYXAl20vrAqq0WA2mluCZgYLeGZDepqWlSFKgQJN+cpYPXbt27flit1orcOIOKT/HM/fgF0MElivnjhzFqyxpn/q2CPSOAB1XHhEccGrnHpwTuMUnvU1LSkralSCX9JOzeOrrr79eh5OCokchi5FsgbMbjuuGV5+3O4TUpGoTr6JUDF3m1CqcMVc5OBLi1fo+PbnTMFAVFhb2KLGnFNmpOGt+kdhgWTm3VaggfloJSsb7ZKkyFIxQ16d+iqrs+DnnoTy7KjhqR2gzITuLV+z/WO2UtiNBQe89+eSTcbBF/qYIQNatW3eB2FALJhiEJNBqZuu5kjgKt7mrJUli61w7z+n+qFCX/JQK5GpXSRBiyukAIVynfadX+oUKBX1PTqRJcbMiAIH1aqrYMwsnwH6f9/wxSpFuZL+se3i7w9S5jmsn5++56T7pyOKwUONjrLyguqB/uFACTqoppaDzc2UHyCOPPHIo3P4jDq9wAgZ7DUy70h4sKXqo8nsGh6crRC1velB0R31D5GLOJ5THuIfam3A8Cn9sdevh+5EecHV1dadSc5IW8DJ8Mg5GiWYqMSPuymRXqbs2wzkOvlwwZeN/vwuJqtfAk40TrGpsEywRqrdGxwT3YJI6ob+91pVeWDEqAhyvFM1l5yCo/iN63tzsQJrQsfD6yoDSe825zQFqet1DgR4VylXCfKI0vSAspNCRfh9nQACFLmWAvN5loB0oQZ0ORZHjoHHp0qUnKTEc2QHS2to6S2ygxnHGPRgcYZhZuj7zUesqHxyC6SylEkuYXp/TCuH3kH2103t2pld5YR1/q8dMwcIDKIIsHulQFo7pmzN9TjrXy04ycBDRk13sHBwPTQAGaiK7O4LU/DZKG3yO2h9qc3eIEJpFrLHAPSJAw3bUZW/RVVJo5sUUbDgxLYCg+u18JfaXrABZsWKFHQp61YiBAhvWkrELEEHNYGDAv+HpDAq6RvMbXvK2pnFQQYlVy6JPFq/U3gTfB142b0C88hc0oIyYgyJ1cylcNEkSJAh3L1u8ePGVcs9RViW9ubm5XlS8wkEcLp45FixYg+PHonidhYOof9iDwp2Iq3K3hcjPesbYwYWwHKzqChWiVM5C+OTlhgEdvdNjokhjZcwebbJRqPYoMvTvwvBTEx7Fl7io58NygkRWgCRLvSLU8BBiG+Qcujx9DQUEvOB+LvMchskWVcPbw+Tvw99ZvxhjoBhMHY4DF6rU8keFa8Bj5aHxwct3e4wI6odybqvYO4Vo2XQKF9aTztWUcvjd3d2S1ZEz3TWyAgSbTfTYI+eG0skqzGU6zaHXMyiYO4T8UXCGID5hCjgZCDjBDR/fWAaDGGV483kUOS2R2zoMvpu5x1eIfH6vD8UOULs76hgkjBhMFKqbR4YtzQBIcpUdYlY9KpUtuuWWW16Ta2SyAsTlcoEvjmw6VF5Sw5sroUe4UOd8AAeZPOAQavVbyLXAiX6aEBE7B550NTIQ1vH4UNQTe0xCYGLU7hBEq71OG4ha0ZLJFDUXgRU6U5IGdQu/jgvUCRCwOFEdZLQTAQj+CiRPGECMVM+GAPm6x7C8lCVytrj0FK6EJ11lyrpw5gN/rO/X0ScDMcdglMMuBg80rouEC+pID4CkAjky6MhqzZJV8EHmREBcpMn6lPR3iAAMnLjxOUO0+10P7YG/Yn8EB2+6rR4DsX8hHZ9C+hTO/UrmHr2IuXpijxl5WGJbP2pGiisO/R7cIKOHK78mOf6BgYGpb7zxBtiPPE3urSsO7lFYlRjXiFDvVr/g5XY1pbE74oosDtwR17A3RnTCx4AP/5uwOqMwFzmWeg8KYW52ww6kIubJ4AhiWZ5sNdJ2H0v78e1j4poLw7Ymi1mOCdBPJPe+HVk8D5ODZtyHrDqIwcC59Ec2PpvNSli+FHUGR9CLQjVrvdA1JICBReIkIPawgb7mKaPZ3jIqDJuoMGIiCzKHcAvx8VlDAKl4ArTe3kWbbE7yGKD1SifbkGudcu6H3YSvdBiRsT2oCo86v2fgVqLn2/SIueKcb3Hugd8i9uqR8+WvLSUUKaghff+OlGJWS0vL13D1ezkTTW6AVFRUNIsNii1DktkF5ZgNv+ABDj/MtK2r4cgbVIp5RPdYIWtYT5N8DjpxoI6m+EqoKox66whwTpbPkDPnnuiaQB1Ir7PB1kOrHK20y+JCjQ1Gv0wTULCbLRCz3OEgWWR9LWY3YN4Pa/t09FSbBdm89hEvihoYkfIZIvTENdBLwqXTyAiApGoo7XdQdqMaeZespMK54N2iHAQG7pi/QdldJHAOd5h2r/KQvyuJLIR/ZvHpIG8JXdAzhSYFHHCkxXJMxUaX3J3JVzF8JiANbJ2rgBa46mgn6vs9VbadvrT1IScVsyO5lkbufnQ0gLMVm106Osoci+odraGymNfkJVq220z+6FAPZsRcTCjLJb5XEOwatbN/hEeeXNZF2IloPGA2FJUVIEjEIHoSIuyDz8GHYDmUAlAqWYPAOfrCtOd9b0pwzHKX0CJnA83xlpM1zi9SgWI4URNAwhbD/TqaFiilH7UdTJ/Yuuil0l20zYbsM6O18yR2QAjw/t8WC02y+6gKHqt8H5wSVDj80QGJ4p4dFmrxD9I74ls+XHIg3mDJtiUAYilGYjy8qqLJHTvI9I5O5GmyKunQQURzE/FcfD3KeaqEDIY4j9Hyjod8XeJaKCvZxzmr6eb2OXSUt4bs4BsIwE8qTkmRN6Z+xDhKAfqa760FUObQYQPlKnY06qg9YKBn2kyCciyWlUlq3rl8z8/rg8Xq/l1m2uYdCg7uN4JCKuFq6NepkGtywAycrOBKbHTI9F6ayzgH3ysrQBoaGlqT8T5fr3LmE/aKd3zso0BfEraLR5/SN4Gu6p5BDiQBT2RNl+NFv68vHZVH7HRD52w6qQ8BzQmrl1wrJVs/OlrRbaK3e2A2BbnyARJ+Bvs6OBBxWYuJPhb8HUMbr1y4EKUG7fEYrGTzNUFHseEllKLhhQk5TZ4mK0BOP/30AXARFL4b2UIergMgz6AH9xI7Bw7POJtxxRoee7irnM7vnUwFUVSwSqFj5DK6BEcpAAAv6Z1KR/cjqFmB+eYyxsS9QeS6fRTy/5dwziFyX3GQMBl8ECCebjXQ270JzjH09cQia3jCMaiWk5o7sPjFliyp9uyzz4o6raXuG/69rADhzh0Oh2iGiYAzlkxN7hbhFDufg2+LdY1/m+QrpKu6ZlCRwDmUjShOgIS51He6Z9JBnhKVgkRHPSED/Q56wHqnciDhJWHO4ce76xmAgy1WwbjpfMhLjrlHQS1FiydJK0bsK+CQE4kGB7Es9UJkB0hlZeVWsbH7IWL5oESLpiOVmm2S77kvjqty7xbnHjb4Ni7vmkYVES5Gw+BQvvEzWLdhTnJp91RyhCTeiMoPKckTdNQHkCzZaaEPYW4VxC0ZxyKIVZw2FJ6xP2430dPtFkid4tstgjJjoYkLEX+VXomPqNEqOVYARBYDlOwAKSsr2yhGZz5158F5CrkAwgsQhqe864skKXawOCf31cHKVKIw3xDfVQyUA4NFdHZPY8wLr8qmIydeIkt22ujFNgN1YzOHsE781s+lJfSNT8Cd7txmQfI3c3Jw4EGhmiMpWsb1XdN4heGSqEm62hoUdVneTLIDZNq0af9ORlxXS0gIGpSnIVQd3EM4wDS8xUWrc5yT9jr+5HlmJr0wz9LTse5aKguirplc004xBMGMOuiT3mh15EEozUO7LfTjTVZ6DqKQGxlFeJPHTlJKD33vdXFlnO//a7ORfrPNSl+JWKsS44op5vUUbjwxFnuVlt0ZCBH0lNRgQrZFWV5LsrChwQtx0003vX3kkUf2gsWNMLV5YYLl03mFNbmHlLLlqm9bkuTQoPyRrioqjOsd6W0Uea9KLF8xKkMtdE6gJyu2S61pRgNgpTZqtFHEWhYLA+ff2fxpBBhR0kqH8AVdyEu6gIt0/l5EwQ7wHUmewWf1dNSBMgOPturpU6Q0nIvQ+OlI9FBniZIdu0TIxjh8TyasUyxKgZFv9+ip2aejzwcMtMHNWyv5+1cAh7WcglO+Dp2CY6/S4B6J0Q8PZBSZFQAiS6YA2QHCY4VHfQOcNceOGDcIyUdX7VXwQeSQAkjwe+BwE5/4E+MepUEzvNy1wjsmA7JntEHTuZgtZgZskhMHJtB7jnZqseJEUJYDSmztCIL1wnzCrmJ27FCRyR4LchPbYIINF5+Qj3T9TWRoW0fGvi0Q+WJJg0cOJQYUDjvnDyfdqTCHaRaAMq0gQmWmKHHecQsHr6H1AVCbkdxto8sAYOjjXvG9uzgpifjukL2WgjOQoZbNuhk3aSLKVVBHEYDMnDlzORJYjwQICNG7JUgl08xkxmspk5fGcBp6OiCuJSm2dOxADZVF2M8tTciM1ybDG3gMpVELHYcxPW7ZlhVABGsQuEOo7CCIIwvivoIkoBg8vgSB8YaOIr4pVDKFQt4uMuxZQ8buL0gPDpMMKPzvHCPVjtoc7T1Eb+ITa8O5UGY0FsBR2EDBad9A9VAGhxhrkiKytLyKdKSi7gapnod/L7sOwg849thjn0k2kAgUQed2pMrJ0UPFZ8XFGodDHeWuHlLsMlOiyHl9wvQ7IYg3fRZN2FA4KBSYcQmFp/OmQqQrHybK9O3C1xshgjlqKDz1bPLPuZYCtUdTxFSUoU8zwZcz488JkAdq5lJw9rcwD/iJknE+KTpJ7B2EM0WR7d0l1U063ysCkAsuuOArk8mE16V4690UwNlvxMZKvwiSziGC8+RizRwxUFkYxUFVwD0Gj686WIAinOm/bQUZHebPQN2x2FBXggMgHzjHKGUKjBFEwhj0WHZs0PDk08l/yHUUmHQGhS1lgniVw5KIrocADPQbKqgn/0yAfPIZEAsz1DmG9MxWA0n1wpnO5k/nGkVELH4wrFlvIGfqZLFBhBHS2PdVgCpms907/U2zty/QiAMgxVpB2Eg2eUzg6dAv7WtKAdqykAWh8tLJewXNASJVcNLpFKk5PDuOkXJkLNYw6YWEZRSZcDQFquaQrnszGbs+R4qdnYKeksXKDF4iBBUaKexohBn3cEHEQ+6n+HLn0jO/OTizRnIow1ndlvbCSFyoGEBOPfXUBwCQ7yZ7ft9WFHI50AQdM/NybILYkQQgDhx04jDEHJdALvru7ccKD3Ilat13WFIDJKZW6ykw8RSK1B4x9Gy27KNikMR1ACRpi9YeTsHqQyjk3EGG9k9IP9BMeljAdMPe2INpO3ybcqRtBFY1NiREKmZRtAgJ4Ni6ljPni08eD9QF+lNSoqioaJdcpFIMIJdccsln0EU2IFeWaGx+cCBKPRC1qg6FMi0X8UCV4hBKvMlf1SFHesesWTw2qcYbLlg7F+CYG5PR89l4HQxIu1MKZZ7Dztn65eniUGzSu9tJF2STMZImcCoYmJKZAzGni1qKYGoGwGxVFIWOFOVgQsG6lo0CLjVhGLiD7pQvwMbGxnVSvaT7vWIA4QHMmzfvYdRKvyvZYJzgIo5GI9krEESYAUgS0oFYv5VBDitRX+MxFYdTA0TQO2DhCTeeFHOGZUATWWcsbGyIX9jk0eJGDLwRoShxk7HwMx54KhCar40fekr8XfhnhVYB8Ss6b2/K6dbW1n4qFz0UfUUhidfdsEcnLY/FouSed30URKRvRgo7aG8uEhk61q4Ccr7aFPSEvccWTf0+iiDdTXAmsmcKjjO5ljiHfhIcgH+yYs/WM3AYnHyDPoEP/2Qg87/z9wmrlFLg4KmEkXfRJXpwde9Ep06dujqHWQ+5VVGA8JMOPfTQR1MNNtjP5ZIDAEhmkQGmAvEdZJXYhHIRLpt+9CmsWIJeVXmwoDTH3r5qQEg2s1TyHlCJIwNSZPtDvcKW888/v0muUSgOkCuvvPK/MNiUWlXPRiRzQwhKJIMoOVslmzxHkqEAMWpjcWsx9wjXw7eab71Drp2Uj374LcI6EOs/SdqECRM+l3MoigNk/vz5boSefJRq0GE4dLmkACeOTteBaC0zkHGY742BUQA/iNpaXHqHdSq5aTJcCj+HrUxtQ1fZeGDfQyRAqjPN06dPf17OQSsOEB7sueeee6fUoP3dUer8xIdDVdIgEWKcTDqylA4FAwOE0/aotSFoVrQJynnlHI17SC0cFHQ9fDSp2uGHH/6iVDeZfJ+X3XTDDTe8iYNUkgmFnVtD0Ec4DEViCoLhREeF9cOVXoSYZ+CtzoRQclzLebXEWthcAmvRAXI8Yvz2wZsCsWP6fs7wLt7MZvNn55xzTrucRMgLQHjA11xzzQ9gynWmHDxo0PGxH6cE04nV0pEDABmZiVKdGghDI6ATD5GIFE0UwtUVM43KuWNGrS9Q0N1B+qBo4hxhVDNmzHhO7uHlDSDf/OY3tx522GF3S00gAtNv63teodRZKn1EiL1D1aqiA4dyEXXCIzZrlz4WZj68RezxwD0p4uzP34ODGDo/HeHVH0ySM84440G5SZQ3gPDAH3zwwV8jDPkdqUmwFa/lbS85dwZS6iQsZpVNtwzhIqkUYannKvk9j8tnGMlBBKEr4XVWcgBjuW9BvPKTwYkYsSTzKCgo+BgvYU47JWvLK0B45FddddX3IGqxKSJl44DG1tU+ASQCJxER39n7bi5A9O5BCQ81MjjGD/NI9Z/f7xkeyPyYRMSKcnSr1pJTAOuv69tBel930msWLly4RAkS5h0gl1566UbE6v8knclEIZG0rfFT33ZwEnYkioEEXKRoInwfMGixq9GlB6DS6TzP1/DYvKKnQLEEfExWa8kpEAmTsW1t0mB8RGt0/uIXv3hMCRLmHSA8id///vd/R7zMs+lMiNOWdnzoxyErnCGBmU9MLzEV6MkxKaaLtJo8Kf0N6TxTiWsEEUsv5uBi7qhGSCtBhSz7RIVbQ//2pOIVjlY8nmXPkreNCkB4VLfffvulFovlC8kR4gI+Qs2cpPsLn1CrfDhIWNQqh5ilx4t4h2VAlQDheYrDgOVr0Zzf6ZBmnF8D2uClaGhfv/ccvciEI/Cz/VYpQowaQBDp67/88suvY/aYzuT4SELXJ0HaAwtXAPU/hEq18R3HALEUGaiwwUDtJnjkVShkCUmuRQ5yCVFXCCuXTq6TDpXG2TW8wM7tZOrekJR7HHDAAQ9DOZftgNRwCo4aQHgg3/3ud9/H8dxvY4OL2z9F1ptLqTWt8FB/E3SNQSKXYNGaaSGfJURBpBtQV4vldXQkCXfXISWPCjE9yiQEOFDsx9i8CtwjSXYOqHVXXHHFLUoOdFQBwhP78Y9//BL8I7/MZJJ82GrPOz5q/QDcBAVzmJskuIgP+ZycKJWmNnMvAyTZgSkdDiPFSnBpusjefYDAVT4CbOjblpR74GDUo2effXZvJnsn02tHHSA8YPhHfjtx4sSMnDy8nzg0ZddrHura4CO/O5bWNIK0JjGAqGu7cfzYAahmJYYBPgOOxMXqGnCmO0nO61m0Alc17XwNnDfpMYieG2+88XtyPlasL1UAhAf23HPPXYeDLvdlOuGQC0GOHwdoxz/d1PwWSq+5ItRmdIOs6nkjJ053zPKVCVVzR8i5UNL1vV9lOvVxej2LViEy7HqTDHwePsks58yZc++JJ56Y2SGiLCimGoDw2J944okbkLb0V1nMQ7B0cdFOPirwYWEnFHXFaZfRMJmD8JFbzroyvPEmMCCbCMIGMupzXF7MR2o7PiVTx8epghI/feihh/ickeJNVQDh2f7lL39ZfMwxx9yMX9NW3IdTaYvVSb16Tg2jrmZGZpNk59INAzgE58Lp5P3ZJ8Jy88BuMu16HTFXyV9wiLn6eb5WVnUA4Yn/6U9/uhemu0ugeCePLUhGIbyOXcYQqs72qk7l5XREh7lRpVVk7Tm1jnHXypiyvt+BJObvICRjMG19gQycPSXJ+k6aNOlv8KEt368BwpO/7bbbnoEZ+D/gTPwyY2KAuhutDBB1WYXYknUUss5bIyPfS8JhL7bY9CELfCKDSMYTH6M3CGWoBsi45XkyetqSggPnzVc/88wz1+RzlqrkIAkC4AzJut/+9reHISzlhYyIgt223dpPXkhpyQ4pZdSfjBfXhwpp3gDy64rFlWG0xu2vChYclWFbRgoM64pfBv5+gOM5MqUIJ4FDued73/veucoNRLxnVQOEh7xgwQLfSy+9dO7kyZM5v1YsHXkarcXioTUF7arjIlxUYMFALZlErFmCsu5pJcOO14X0NuNe1GJw+HrJuOkpMjm3pky2gSSEN1122WWynhZMYxup+AD3sNE/9dRTt8Lr/g0kxU4r51EU0H/H0UoIcVTNyzhRV31KoJgOQeVdMV2E31imTqT9bHpb8CTHHIjjrCUKGLrayPTlE2QaSH7Og2eOcJJfLlmyJGX6KKUopOYDeEnnfMcdd1z88ssv/yIYDCIjcvLGRbhu2XMwHe6rEpLJqWGyLFmxj6ZD76E/1H6CojoIVBQZGF8XLJ9NoSlnI5FcPI2nKmaQ41YUzvawKfcLMm97kfRIaZNqXeDv+BlMuooFI0rNRg17RmqMot+vXr3a8uc///l/N23ahGITSRrW4oiBCvpBx9fIhnwn6oAIqxfw+OOzxtZGf67ZQCFD0jhfCjkmIsv7IiJOAp1tPY2sKKzATULl1QDpWz8iU9NKgANl4lI8Bharu6CU36rASNLucswCJDFDeFPX9vf3o0aAeOMzSje2zaZ53pp4UZ20aaPohQwSFv+WlW2iFSV7kARa/HECxzEWULBhQawUglBCYKwtG5tx4/oGPORGiJBciSTVLHA0+9kVK1Z8U9FFSKNz1SvpUnNYtGjRvamuYYvqc2U7yKXj+Cz1yPPMzVhhv6RnGs1yl8T0EVHLFhT3kJssO14m0+cPIwANDkX2uI8FX0mi/G0ItUba15H5879Dv1qPF1VqcBQWFi5XAzh4X421V5EoFo4//viP3G43immIN65TfkPbLJrvqRHKEKhH1IqJW20GNz1QuYE2FSArUooViXETu5DDNzzhGOTxLVav2CUo4gAy8liZmhBXhYBMvXRlKC4A+8TKlSsvlnox5uv7Mc9BmFA45/4z8fdvjIycS+7Fkp3k1PlVZfaNBTHqqCZsF/Sk6R5s+BS+Tb5ej8BGc+tqsnz2IOl3wvPOpQDUxFESwHDBXA3fhmXDMjI6t6UFDuTVvVdN4Bg3HIQnAi6yClxEtLKugBJwkYu7JtPX+w9QTYHPwW9BVtr7AeC7qz+lzRKcRAB94oOEc2FYu8I1h6Es9IRYXQ+BC+VTOGDLFA8IHGOgDUdk15KxI3ZMNhHJLPXGhyn3188+++x/Sl2X7+/zSUVF54YzJScuXbr0X3hI0hw6JQEz3dI6h6YES1SlsCcIwyDphvn3+ZIdQl11jxEbLo0VEvYmgBEumYr66TMpgnLPZCqM1+xAB0pkjI+bawXuxSUJUK7N0L0Rn03gFunXN4SHvAnO4JtQSuZ5RTdIlp2nQf4sex6F2xCacuf69etvS/po7KRqv5VubT2EJoQL49qIekiQ8JEwUHbjTMurxU20prCD3Ai+3NskdBSBWcLqxdkaI44GgGUSRQvBWbhCLr/hGSxC9SqWruOd7e0z8UucIwwhJP9bzIdBQfgu3G2kRyI3PeoZ6r2dguiXKd+qq6t7/tprr73wrLPOyjpyW+ltpp7dIdNMkUBsZW9v70mpQDLF7aAbIfNXR+xxB6K6yBATn9jmFqEm4wCtt3fRBwDKLosLJybTlJ7iugyfhjcZS6gEc3XpfIh0jlKwGMCpmB1PmI0NL/hXBtVeZ67AB2vgs9AhTopQNFOPvLg6lB7Qo14hF9EU6hVKWKNSLGnH0Ucf/RP4sR6WadkV60ZdO0OGaS5fvrzhV7/61buRSATF9URaXHhnB+L3u2ZRQZSD0NVpq0hEIzNHcSOvxWZLL31p7aPNtj7qNKF0HcxzbB1OyPn80wCHClfTbfAXUEOwkGqCduHD1X/dyA28zdxPz5fupF2FOFJmLcNGR2AkA0RIng2QCEo2HHhI9SlwHAbJIPN4rhsGKUJf+9a3vvUdtGYZllvxLnKdr+IDzOYBAMjF//rXvzjTnvj8sAc4Q+nCvgl0Wc9UsgpVRdQJEp5/LHFp7DcGC8coBwCOkACQmE9BiPOCuQ5FtcmCg1kxc3bs3weLPnzHOksX/XHCZxTC+f1MxaJs1oPvMRqNW8E1br/33nufyraP0bhvXAKECXnxxRf/ecuWLdenIip72b/ecwCd5zxQcNqpJV5LaiMMtwSns4gJsc2Pw8j/U/U5feRAeuR0bpQajMT3OPTWhKpPDzz22GO/y7GrUbk9DyQalXkJDz3ttNNe6ujoOCPVCIxhHUAykc5xTiKusB4DyfgiS4wDRXFWIERPl26j5WWQbhSeIoDRPGvWrHuWLVt27+jtgNyfrDCZch9grj3gHMG/vV7vgqT94NVqgCC/qK+eLu6dOq5AMlgsG0CozV8rvqQPHZ1J475ypbUgrul022pqav6JMzw/kqO/0e5DvYK3TJT55S9/eR6Oaq5N2h1eEWFQ4bXS3fRi8U5yx08hpnBoyzQy5bthrhFCQORXpj76Xc16+qBIMXB0ORyO5YiLO33t2rVTxgs4BMArv0yj/wR4aCfec889//T5fAenGg3HbM3ylNCVXdOpPuQYpuaO/jzSGcFgyxdzjTcdAH7prpgvRb7VdqIe4FfgFB9NmTJlxQknnPAcMo2Mh3fKCBLLR7J0Vm8Ur0HOrckAyZPhcDhpaLwwPCxzKTzuZ/Q10sKBepwj4brrY0crYSsVc8G3AIyVRbtpjwWnlHOTE7ogNrXDCtVRVla2bu7cua8efPDB7yOj+n6Rkn6/AQjvffhISn/zm9/80+/3H5cSqwAJW0DnuMpgBp5GdaGCvVHAaiRYQgnnrPat8MA/XLGJNtj7hCDNLLiGr6KiYmV9ff1byAOwGtku15x//vnqOSeQ55esGtdbURIgWtQGkDzjdDpPl3wQQFIUNNFZvY10vKuOSqLWvV4FNRAuIU6FwTV2wuP+CkJT1hV2CXnBsgAGIS7qy/POO++6n/70p6skabOfXKCGdR4VUp9++ukPtre3f0fy4XGn4gG+Qjq/50Ca7i8he9z7HnPF5b+xczBxInEHPOMf2TvoreLWfTFbWQwLItQ730a77rrrtuV/Rup9YhakVO9kMh0Z9sM1n3766W9wH+owS7S42FXvK6BFzgY63o3UPRC8uA31WUt1lPn3Ce03xjFQyg22qfcL2oSsLVtt/bHCpdmJU8JgysvL73v99ddvyHxk4/+O/RogvLxQ3Kc8+eSTT4RCodTKe2IvxDnKVG8RTfeW0MHecpoUKCKbEN4RczRyk4OwieQOLEJ5dCFibvGJvZs2IRZrhxWl5nIARXw6bbNnz/4VnHlLx/9Wz26Gcqxjdk9W2V3wuj8Kr/tlGQ0r7mSsDdhpnquajnJXUykyuFuiRiG6a3AslBRoEqEgMd9FLNbKg6KfTaYBer+wnbYiIXeX2UdCvjkZVg1m2lXQN75/yy23pFUnMiO6jKOLZSD1+KEG5O+L1q1b9xNEAh+S8ayww61h1ElEeYOSsIXqEU071VdElSEbFSKSlqNpObs7f1giiuAPjsZlILhRutoNMLQZvbQJOYVbYZrlIkAMEK8BcJEJFPE57ZwxY8b/IDbqnoznuB/eoAFEZNGvvvrqG6Gb3IqKVThplEPbpzzsVREskVicLUfiMkDi0ff7uIJyK+KHrrEM+W1vgQ8DMe5aS4cCyi1HOk9X8TUvvvhi4cMPP/yHpqamcwGUGhUPVXJodrv9HZza+9mtt976nuTF2gVDKKABRGJDvPDCC2WPP/74L7dt23YRgCJt7VLRBkMKHY6P+i8A4wMVDWtMDUUDSJrLhQNYhUgMsaS1tfVUAKU+zdtG47KBqqqqV04++eQ/QQHXOEaOK6ABJAsCorjPOatWrboRYfRzcXvSLCpZdJ31LQaDYSPCQv7vzDPPfBSHxVqy7ki7UROx5NoDiBIuA2f5DsSvMwGWqeAsqIyTt+aHqXZLdXX1uxCjHoLy/VHenrwfPUjjIDIu9uLFixd+9tlnF0AMOyIQCNQDMJUydu9COEgzAgk/wEm9V+bPn//2Oeeck/eCMjLOZ0x0pQFEwWWCct+wcePGQ9va2ubACTnD5XLVoqZJIaKJC+G5R5p2IVYlsQYRhJUHYXHqhrjUg9qMHQDDNqTj/LShoWEzxKdd4BTqK92rIP20rjUKaBTQKKBRQKOARgGNAhoFNApoFNAooFFAo4BGAY0CGgU0CmgU0CigUUCjgEYBjQIaBTQKaBTQKKBRQKOARgGNAhoFNApoFNAooFFAo4BGAY0CGgU0CmgU0CigUUCjgEYBjQIaBTQKaBTQKKBRQEYK/H8xbazvzq6r4QAAAABJRU5ErkJggg==`


var endpoint="studentvuebackend.awesomecrater.repl.co"

if(!folder_exists) {
    await w96.FS.mkdir("C:/user/appdata/StudentVue")
}
if(!login_exists) {
    await w96.FS.writestr("C:/user/appdata/StudentVue/LoginData",JSON.stringify({
        loggedIn:false,
        username:null,
        password:null
    }))
} else {
    try {
        if(JSON.parse(await w96.FS.readstr("C:/user/appdata/StudentVue/LoginData")).loggedIn) {
            loggedIn=true
        }
    } catch (error) {
        loggedIn=false
    }
}

if(!server_data) {
    await w96.FS.writestr("C:/user/appdata/StudentVue/ServerData",JSON.stringify({
        endpoint:null,
        serverName:null
    }))
}

class StudentVue extends WApplication {
    constructor() { super() }
    async main(argv) {
        var hasServer=false
        var serverData={
            endpoint:null,
            serverName:null
        }
        try {
            serverData=JSON.parse(await w96.FS.readstr("C:/user/appdata/StudentVue/ServerData"))
            if(serverData.endpoint) {
                hasServer=true
            }
        } catch (error) {
            hasServer=false
        }
        var loginData;
        try {
            loginData=JSON.parse(await w96.FS.readstr("C:/user/appdata/StudentVue/LoginData"))
        } catch (e) {
            loginData={username:null,password:null}
        }
        var messages;
        var mainwnd=this.createWindow({
            title: "StudentVue",
            center:true,
            initialWidth: 720,
            initialHeight: 430,
            taskbar:true,
            icon:iconSmall,
            body: `<div style="width:100%;height:100%;font-family:Arial,Helvetica,sans;">
            <div class="synergy-oobe" style="width:100%;height:100%;background:linear-gradient(217deg, rgba(204, 149, 10,.4), rgba(153, 26, 156,.8), rgba(204, 149, 10,0) 70.71%),linear-gradient(127deg, rgba(0,255,0,.8), rgba(0,255,0,0) 70.71%),linear-gradient(336deg, rgba(0,0,255,.8), rgba(0,0,255,0) 70.71%);">
                <p style="color:white;font-size:50px;margin:0;"><img src="${iconLarge}" style="height:58px;vertical-align:middle;" /> StudentVue</p>
                <p style="margin:0;color:white;font-size: 24px;">By Synergy</p>
                <p><button class="synergy-oobe-choose" style="position:absolute;left:0%;bottom:4px;color:white;background:blue;width:100%;border:none;outline:none;padding: 4px;border-radius:8px;">Get Started</button></p>
            </div>
            <div class="synergy-select" style="display:none;overflow:auto;width:100%;height:100%;background:linear-gradient(217deg, rgba(204, 149, 10,.4), rgba(153, 26, 156,.8), rgba(204, 149, 10,0) 70.71%),linear-gradient(127deg, rgba(0,255,0,.8), rgba(0,255,0,0) 70.71%),linear-gradient(336deg, rgba(0,0,255,.8), rgba(0,0,255,0) 70.71%);">
                <p style="color:white;font-size:50px;margin:0;"><img src="${iconLarge}" style="height:58px;vertical-align:middle;" /> Find a school near you.</p>
                <p style="color:white;font-size:15px;margin:0;">Search for your school by zip code <input class="w96-textbox synergy-select-zip" /> <button class="w96-button synergy-select-search" style="color:white;background:linear-gradient(45deg, green, lime); border-radius: 8px; padding: 4px; font-size: 18px;float:right;margin-right:5px;margin-left:5px;border: none;outline:none;">Search</button>
                <div class="synergy-select-list">
                When you search for nearby schools, they appear here.
                </div>
                <div style="height:80px;width:100%;"></div>
                <div style="color:white;font-size:15px;display:flex;flex-direction:column;position:absolute;width:100%;left:0px;bottom:0px;">School not listed? Endpoint URL: <input class="synergy-select-uri w96-textbox" /> <button style="color:white;background:blue; border-radius: 8px; padding: 4px; font-size: 18px;float:right;margin-right:5px;margin-left:5px;border: none;outline:none;" class="synergy-select-login">Continue to Login</button></div>
            </div>
            <div class="synergy-login" style="display:none;overflow:auto;width:100%;height:100%;background:linear-gradient(217deg, rgba(204, 149, 10,.4), rgba(153, 26, 156,.8), rgba(204, 149, 10,0) 70.71%),linear-gradient(127deg, rgba(0,255,0,.8), rgba(0,255,0,0) 70.71%),linear-gradient(336deg, rgba(0,0,255,.8), rgba(0,0,255,0) 70.71%);">
                <p style="color:white;font-size:50px;margin:0;"><img src="${iconLarge}" style="height:58px;vertical-align:middle;" /> Synergy Log In</p>
                <p style="font-size: 18px;color:white">Username <input type="text" class="synergy-login-username" /></p>
                <p style="font-size: 18px;color:white">Password <input type="password" class="synergy-login-password" /></p>
                <p><button class="synergy-login-ok" style="position:absolute;left:0%;bottom:4px;color:white;background:blue;width:100%;border:none;outline:none;padding: 4px;border-radius:8px;">Let's Go!</button></p>
            </div>
            <div class="synergy-app" style="display:none;overflow:auto;width:100%;height:100%;background:linear-gradient(217deg, rgba(204, 149, 10,.4), rgba(153, 26, 156,.8), rgba(204, 149, 10,0) 70.71%),linear-gradient(127deg, rgba(0,255,0,.8), rgba(0,255,0,0) 70.71%),linear-gradient(336deg, rgba(0,0,255,.8), rgba(0,0,255,0) 70.71%);">
            <p style="font-size: 15px;color:white;margin:0px;">Hello, <span class="synergy-welcome-name">{SYNERGY.INFO.FULLNAME}</span> <a style="color:white;text-decoration:underline" class="synergy-app-logoff">Log out</a></p>
            <div style="font-size:15px;color:white;margin:0px;display:flex;flex-direction:row;width:100%;">
            <div style="position:relative;width:24px;" class="synergy-tab-home">
            <img style="height:24px;width:24px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AcVFyo0WMFH9wAABKBJREFUaN7tWU1sG0UU/na9TmzjpOsYG1dIqaPGSUycspZQFZVDHKSqAleqI6CXgGSLA7TQYvfCCUHFBU4JUgtCDbIlDqiU1gk3Lti9VEggxUEVCGixaaUQ0kNNigQpdh4HjyvH8e5s4t9IHevJ9szbmXn7vvnmvRmBiLCbi1RvB4fe/kat6V0A77DfkwByABQAxwCEAMiVytfee0ZoiwEEXR5MdawHQLscQtRmCx56oN0sVr8HdjuEtumAPIB5AAuMVjOMTpVOh1CMTTxX3fDd+0fyANKdbkCm1uQfroEmekAG8GQrDBAbsYirRCFCigihiroZVr9FvwMMoEoJE1GKiJSqeoXVh6vqO8qAOBO5epJM5AqdjjLATUSLtd6uioSZvrvtBhyIfRVik1F0Tr4SUotEFGqbAWPRhRkiSmpABkSU0GiTiSg5Fl2YqccAYbtu9L05LwNIAghoqOUATLENTGH6bg39NICp6x+G8k31gO90MgCiLIgCtfiTSRpEfhDJTFdm/9MazwRAlPWdTgaaZsDoqStRRoVakDlLRJNEVNZ1s+8oqz/LgVRq9NSVaEMh9MQbl2UAcZaIa0WZZciowSvNdMqQkjX6mwcQ+fHc8/m6DPC+/qUe/GbYqYNbh255beRYoq/wdH86/0JmRxAaOXlpE1+ryOyeg25/NbdryIM9gz03y9MdOXkpvC0PDJ/4QgYwAyDMgUyEwYIHL02YMLjFOZBKAIj9/PHxPDcaJSKeazMMBrImDAQBJlcvAODflbVakVuIwS0CwM/gpzZumLX5uRDi7KoJxiYBlaCtFN8YBDi8ezF9eATTh0fg8O6FIIlqO3KK9TfJ2fgUXfkA0YYaZMppoSa8RKMBrvH9CO63461DpfXsc1rxgc2ClW9vYOO/Yq3cIQ5ggo1xlY0h7yihIaJZANEazJHXwRx4/KkBfBL0YqjP8qAuOPgoPH0WvFoo4ta1X8GByRRjtWpGm9XFQjcvTMeIKEJEaeZSfwV7aAZtYrcEY5e0afLlMtRngbFLgtgt6Qny3GzcBJtH5OaF6ZjulPK3uZcSbOVj4JXPynsBt5ice3B81KXa7rQY8Xu3hMI/97W6KcdakeynL0fqDiU4ocNm4ST4Y4/1QHqkW2/I7W5IUr+tYJWja7d0QTAY0MjTSD0e0C02Rw88NfBfLp4+C2yOHt39NcgD+joSJQMEqxkT/ersN9EvQ7CaIRhEbBSKneUBu8eF4KCdO2Bw0A67x9VZHjD2mNE/4MCZ8X1c3TPj+/DDyj3kl+9i/e7fLUjq1bMogAhGqwnKhBfxYz4YRf49nVEUcO45Lw48PQSj1aTdfysgZHb04uiwEzaT/lNKm0nC0WEnzI7eFkCIw433bt/B3NIy5paWt7T1SqX3s1bYUH223ju2uveBwnoBua8Xt9QPHvHj8oul891nP1/EjRo6rTmd3uGuI67fx0ff34bd0gWpUASadJdWN4TUyi9Xr+OP7J8wSBLWbq027TpWapIDQEXCX9nVDrjgaN81aq4hNLp68bU0gXKlWLNlnzyB5ht3MkeYAiEHYhFncyUHwuSdiyd0nZP+DzwuQVPFMXRQAAAAAElFTkSuQmCC" />
            </div>
            <div style="position:relative;width:24px;" class="synergy-tab-messages">
            <img style="height:24px;width:24px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AcFFRgDXpE1eAAACP5JREFUaN7VmltsW/Udxz927Dix40vqW+KkviSuKY0bqAYt3QMb3dQLpVBA2voyaRISU1+4PTCBNk28rNsLbExaBBraHtAE0iiXEpr2YZOQ0ERBE9WSNHFaJ3biOI7ja+JrTs7ZQ30c5560Sdv9JMvy/5zz/3+/5/c739/3f2QFgCRJbGdcvnRJA7wFnAV6gJePHjtWYgdCsd0ELl+6dLQC/HTN8N+AnqPHjl25pwlc6ut7Ffg9wG6nE5fLRSgUYjwclk/5xbHjx9+9Jwn0Xbz4W+A1gI7OTnw+X/VYIBAgeOOG/PPc8RMnXr9nCPRdvHiwUjI/B+jy+9m9ezfiwgKZbBYAk8nExMQEA/398mU9wB+OnzgRuKsELn7xxfMV8A+2tbfjcjoxmkzMl8tk0mnEyrxKhQLTrl2k02nCoRCxWAzgO6DnxOOPv3tXCHzR21stGafLhd/vByCfyzE7O7vqNQajEY1Gw+DgIOFQqFpSj588+fodI9Db22sHflO58+zdu5fOzk5EUSSbzVLI59e9vlGrxWQyMTw8zPXr1+XhAeDEyZMnx3eUQO/nn1cl0mw243K5aHU4EASBZDKJUC5vah5VfT0Wi4XY1BShUIhEIgEQBf508oknzu0Igc8vXKhKpMvtxufzodFoKJVKzMTj1XrfbCgVCmx2O+VymWAwSGhsTD50/olTp57dVgIXPvvsz3LJ7Nmzh7333w/AbDZLMpm8LRWxWCzompoYunaNkZERefg/p5588nu3TeCzTz+tSqTRaMTtduNyuxFFkWQisebDutVwud0olUpCY2OMjY2RyWQAhoBfP/nUU/+4JQKffvLJ88A7AE6nE7fHQ3NzM8L8PNGpKcql7bM29RoNrS0tqNRqUqkUY6OjhBe79/tPnT79sy0R+OTjj6sS6fP52NfVdVMi83mikQgLorjtpqxOqaS1rQ2tVgvA4MAAgUC1z/379NNPf39DAh+fP79EIv1+P3sqliCVTMoNaEfDbrfTvGsXACOBAP2L3XsAOPP0M8/0r0rg/Ecf/aQC/IcOhwNPRwd2u52FhQWGh4aIx+OUSqUdJ6DRaLBardy3dy91dXXEYjFGg0EmJycBZoB3nnn22V9VMwew3+9/tVLv7o7OTrq7uzEajZTLZfr7+4lOTiIIwvpqoFBUv5VK5ZKPQqGoHpdjLeEQBIFsNstcLofZbMZoNGK12RAEgVQyqQUevTY4eHDfvn1/B1BVJqt2Vf/+/QDMzc4SHB0llUwiSdIKh7lTITvXVDLJ0NAQHR4PTXo9Bw4cQK1SMTQ0BHC02k8qBNySJFXBx2IxhoaHKZfLFItFRFG8I+Bl0RBFkWKxSLlcZmh4uPrs+ffvR5IkJElSVbv68nQGg0Hi8Xj1t7gDirNRyGsuLCwAMDY2Ri6Xo6OjY0XpqYAlNkAUxeqFy49tFNN5gQ8CKfoTBeKFpc/MwRYdpzwG/ObGjQlU1lyCo0JKPtYz/CZn73ulkoGau+z1etHr9YxUNHizGfggkOLDQAoAa6OKLnMDfnMj03mB0WyJK1M5rkzleKxdzwsPWjeVAbFCYI/Ph91upxbr5UhvDYFld9lut6PT6bh69SpI0oYkZPBalZLnuswc2a1fcc5otszb303zr4lZbFoVZ3zNaxNYWLipXkolDzzwAE1NTSvVS1rjGZCjqamJQ4cO8e033xCNRtdcrD9R4MNACmujircebSc3L3Lu2xj9MwU8xnqe67Lw3sAMAGd8zbw3kODDQIoj7XpsWtWaktzS0sJDDz+MSqVaVX7lTMgqtCoJlUrFI4cP0+n1kk6nV3eqozf3vS88aCU3L/LylxNcmcqRF0RGM2Ve+XKCgUSRgUQRm1ZdvfP9icKq86XTaTq9Xh45fHgF+FqsoihtTECO7u5u8vn8EnUCyM2LXJnK4TbU4zc38vbVafKCiLVRxfvH3Lz7IyfWxkUQnsp5ANOFlY0xHo+Tz+fp7u5eE0uVgCRungCAw+FAo9HILb1S1zetxaEWHdN5gYFEEYDnuszo1Ep0amUVcJe5oQJ8ftX5Jycn0Wg0OByOdXEsZqCGgChJm5JLg8GA1WolEoksGfcY6peUxKEW3aK0VgB7DJpK6dwkqVMrq+dEIhGsVisGg2FTEisuJ7CZDMihVqtpa2tbkon1+oKcFb+5gdFsmQvBzE2S9kWSbW1tqNXqTa2/aga2QqC2pBpKsyiQ6E8Ul9z1r6dy5OZF3htMrJDRvCDyU18zNq0KQRDWVbj1CAiiUCOjt2gXvA4bLxYV/CWQ5YyvmYMtOq5M5fjdtyv3DXKTe6xdzxlfM3Nzc5TLZVpbW7dGoILVprGt3wc2Gz/osKJvqOev/XFee8jOP8dn+TqWx2Oo51CLjgvBDNOFeXTqOo60N2HTqjn/3Sg/9trYVdm4bDUDAL986I3tITA3N0eLNMfZ7hYAjuzWL+nEy21DKpXi0XY9KqXiltaTsR6wPnz7BJLJJMVikVaHY8WGZa1obl60ELOzs+j1+lsisHw/sCUSoigSiUQQBAHHFsCvtn2cmprakmWXsfYMv3lrBAqFAuFQCJ1Oh81mu73XKfX16PV6wqEQhUJhSwQuR3q3TiCdTjMZiWCz2zGZTNuyedHpdNjsdiYjkTX91moEZDeqlJ2ptEHJRKNRspkMTper+u5mu0Kr1eJ0uchmMkSj0XVLSsYqbbaRlUolJsbHUavVOF2uTXfMrUbt/BPj42u+wtmSG81kMoyHwxiMRiwWyx3ZD1ssFgxGI+PhsPyOdF03qqqMrvQxsRjJVAqT0bht9b7ZMJlMFAsFotEopWIRW2U7WYtVLrMVfUBcWCAUDlMqlWhtbcVoNHI3oqW1lUatlmg0Si6fx+V0oqyrq2JdSgDSgCmRSDAzM4NCocDj8aDRaLibYTQaaWhoIBQKMXL9OhaLBQkoK0pL3SiS1IMk8dVXX4Ek4fV67zr42mbn9XqpxXdDd22ZG5WkPwKm0WDw7GgwSF9fH/dq3Gi6RkD3X4XsRpUAL770UkyCNyQ4J0FaqtXbe+RTVpYZMQ0qbjQPKWxGe9WNbvufPe50KPk/j/8BtdnlznyCDZIAAAAASUVORK5CYII="><span class="synergy-inbox-count" style="border-radius:4px;position:absolute;bottom:-2px;left:17px;color:white;background:red">...</span>
            </div>
            </div>
            <div class="synergy-view-home">
            <img style="height:150px;background:blue" src="DEFE" class="synergy-student-pic" />
            <p style="font-size:48px;margin:0" class="synergy-enlarged-name">...</p>
            <p style="font-size:24px;margin:0" class="synergy-enlarged-unreads">Loading messages...</p>
            </div>
            <div class="synergy-view-messages">
            Messages loading...
            </div>
            </div>
            </div>`
        },true);
        var dom=mainwnd.wndObject;
        dom.querySelector('.synergy-oobe-choose').onclick=function(){
            loadSchoolSelect()
        }
        if(hasServer) {
            if(loggedIn) {
                showApp()
            } else {
                loadLogonPage()
            }
        }
        function showApp() {
            dom.querySelector(".synergy-oobe").style.display='none';
            dom.querySelector(".synergy-login").style.display='none';
            dom.querySelector(".synergy-app").style.display='';
            dom.querySelector(".synergy-student-pic").src="";
            dom.querySelector(".synergy-select").style.display='none';
            dom.querySelector(".synergy-view-home").style.display='';
            dom.querySelector(".synergy-view-messages").style.display='none';
            dom.querySelector(".synergy-app-logoff").onclick=async function() {
                await w96.FS.writestr("C:/user/appdata/StudentVue/LoginData",JSON.stringify({
                    username:null,
                    password:null,
                    loggedIn:null
                }))
                loggedIn=false;
                loginData={
                    username:null,
                    password:null,
                    loggedIn:null
                };
                loadLogonPage()
            }
            dom.querySelector(".synergy-tab-home").onclick=function(){
                dom.querySelector(".synergy-view-home").style.display='';
                dom.querySelector(".synergy-view-messages").style.display='none';
            }
            dom.querySelector(".synergy-tab-messages").onclick=function(){
                dom.querySelector(".synergy-view-home").style.display='none';
                dom.querySelector(".synergy-view-messages").style.display='';
            }
            var xhr=new XMLHttpRequest()
            xhr.onreadystatechange=function(){
                if(this.readyState==4) {
                    if(this.status==200) {
                        try {
                            dom.querySelector(".synergy-welcome-name").innerText=JSON.parse((JSON.parse(JSON.parse(this.responseText)).StudentInfo)).NickName;
                            dom.querySelector(".synergy-enlarged-name").innerText=JSON.parse((JSON.parse(JSON.parse(this.responseText)).StudentInfo)).NickName;
                        } catch (error) {
                            alert("We couldn't load your info because you don't exist.")
                        }
                    } else {
                        alert("We couldn't load your info because you don't exist.")
                    }
                }
            }
            xhr.open("POST", `https://${endpoint}/api/student`)
            xhr.send(`?username=${loginData.username}&password=${loginData.password}&server=${serverData.endpoint}`)
            var mxhr=new XMLHttpRequest();
            mxhr.onreadystatechange=function() {
                if(this.readyState==4) {
                    try {
                        var m = JSON.parse(this.responseText)
                        var u = 0
                        messages=[]
                        m.forEach(function(e) {
                            messages.push(JSON.parse(e))
                            if(JSON.parse(e).Read==="false") {
                                u++
                            }
                        })
                        dom.querySelector(".synergy-inbox-count").innerText=`${u}`
                        dom.querySelector(".synergy-enlarged-unreads").innerText=`${u} Unread messages`
                        var mc=dom.querySelector(".synergy-view-messages");
                        mc.innerHTML=""
                        messages.forEach(function(message) {
                            var element=document.createElement("div")
                            element.innerHTML=`${message.Read==="true"?"":"<b>"}${message.Subject}${message.Read==="true"?"":" <span style='color:red'>UNREAD</span></b>"}`
                            mc.appendChild(element)
                        })
                    } catch (error) {
                        dom.querySelector(".synergy-inbox-count").innerText="!"
                        dom.querySelector(".synergy-enlarged-unreads").innerText="Unable to load messages"
                    }
                }
            }
            mxhr.open("POST", `https://${endpoint}/api/messages`)
            mxhr.send(`?username=${loginData.username}&password=${loginData.password}&server=${serverData.endpoint}`)
            var pxhr=new XMLHttpRequest();
            pxhr.onreadystatechange=function(){
                if(this.readyState==4) {
                    try {
                        var si=JSON.parse(JSON.parse(JSON.parse(this.responseText)).StudentInfo)
                        dom.querySelector(".synergy-student-pic").src=`data:image/png;base64,${si.Photo}`;
                    } catch(e){}
                }
            }
            pxhr.open("POST", `https://${endpoint}/api/student`)
            pxhr.send(`?username=${loginData.username}&password=${loginData.password}&server=${serverData.endpoint}`)
        }
        function loadLogonPage() {
            dom.querySelector(".synergy-oobe").style.display='none';
            dom.querySelector(".synergy-login").style.display='';
            dom.querySelector(".synergy-app").style.display='none';
            dom.querySelector(".synergy-select").style.display='none';
            dom.querySelector(".synergy-login-username").innerText=loginData.username?loginData.username:'';
            dom.querySelector(".synergy-login-password").innerText=loginData.password?loginData.password:''
            dom.querySelector(".synergy-login-ok").onclick=function() {
                var xhr=new XMLHttpRequest()
                xhr.onreadystatechange=function(){
                    if(this.readyState==4) {
                        if(this.status===200) {
                            try {
                                JSON.parse((JSON.parse(JSON.parse(this.responseText)).StudentInfo)).NickName.length;
                            } catch(E) {
                                return alert(
                                    "For some reason, we couldn't log you in.<br>This is probably because you forgot your password and set LassPass to mobile devces only.",
                                    {
                                        icon: 'error'
                                    }
                                )
                            }
                            loginData.username=dom.querySelector(".synergy-login-username").value;
                            loginData.password=dom.querySelector(".synergy-login-password").value;
                            loggedIn=true
                            showApp()
                            w96.FS.writestr("C:/user/appdata/StudentVue/LoginData",JSON.stringify(
                                {
                                    loggedIn:true,
                                    username:dom.querySelector(".synergy-login-username").value,
                                    password:dom.querySelector(".synergy-login-password").value
                                }
                            ))
                        } else {
                            alert(
                                "For some reason, we couldn't log you in.<br>This is probably because you forgot your password and set LassPass to mobile devces only.",
                                {
                                    icon: 'error'
                                }
                            )
                        }
                    }
                }
                w96.FS.writestr("C:/user/appdata/StudentVue/LoginData",JSON.stringify(
                    {
                        loggedIn:false,
                        username:dom.querySelector(".synergy-login-username").value,
                        password:dom.querySelector(".synergy-login-password").value
                    }
                ))
                xhr.open("POST",`https://${endpoint}/api/student`)
                xhr.send(`?username=${dom.querySelector(".synergy-login-username").value}&password=${dom.querySelector(".synergy-login-password").value}&server=${serverData.endpoint}`)
            }
        }
        function loadSchoolSelect() {
            dom.querySelector(".synergy-oobe").style.display='none';
            dom.querySelector(".synergy-login").style.display='none';
            dom.querySelector(".synergy-app").style.display='none';
            dom.querySelector(".synergy-select").style.display='';
            dom.querySelector('.synergy-select-list').innerHTML="When you search for nearby schools, they appear here."
            dom.querySelector(".synergy-select-uri").value="";
            dom.querySelector(".synergy-select-zip").value="";
            dom.querySelector('.synergy-select-search').onclick=function() {
                if(dom.querySelector(".synergy-select-zip").value.length<3) {
                    return alert("ZIP codes must have a minimum length of 3.")
                }
                var xhr=new XMLHttpRequest()
                xhr.onreadystatechange=function() {
                    if(this.readyState==4) {
                        if(this.status==200) {
                            try {
                                var list=JSON.parse(this.responseText)
                                dom.querySelector('.synergy-select-list').innerHTML="";
                                list.forEach(function(dalue) {
                                    var value=JSON.parse(dalue)
                                    var element=document.createElement('div');
                                    element.innerText=value.Name+"("+value.Address+")"
                                    element.onclick=async function() {
                                        await w96.FS.writestr("C:/user/appdata/StudentVue/ServerData",JSON.stringify({
                                            endpoint: (value.PvueURL.endsWith("/") ? value.PvueURL.slice(0,value.PvueURL.length-1) : value.PvueURL),
                                            name: value.name
                                        }))
                                        serverData={endpoint:(value.PvueURL.endsWith("/") ? value.PvueURL.slice(0,value.PvueURL.length-1) : value.PvueURL)}
                                        loadLogonPage()
                                    }
                                    dom.querySelector('.synergy-select-list').appendChild(element)
                                })
                            } catch (error) {
                                alert("A JavaScript error occurred. Check `systemlog` for more info.")
                                w96.sys.log.events.push({
                                    source: "Synergy SOAP Front-End",
                                    severity: 'critical',
                                    date: (new Date()).toString(),
                                    message: "Failed to pull zip codes: "+error.name+": "+error.message
                                })
                            }
                        } else {
                            alert("Unable to connect to the server. The server may be down.")
                            w96.sys.log.events.push({
                                source: "Synergy SOAP Front-End",
                                severity: 'warning',
                                date: (new Date()).toString(),
                                message: "Non-200 Proxy Response: "+this.status
                            })
                        }
                    }
                }
                xhr.open("POST",`https://${endpoint}/api/locate`)
                xhr.send(dom.querySelector(".synergy-select-zip").value)
            }
        }
        mainwnd.show()
    }
}

return await WApplication.execAsync(new StudentVue(),this.boxedEnv.args,this);